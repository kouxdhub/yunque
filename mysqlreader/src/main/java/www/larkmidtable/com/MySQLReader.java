package www.larkmidtable.com;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import www.larkmidtable.com.reader.AbstractDBReader;

/**
 * @Date: 2022/11/14 11:01
 * @Description:
 **/
public class MySQLReader extends AbstractDBReader {

    private static final Logger logger = LoggerFactory.getLogger(MySQLReader.class);

    @Override
    protected AbstractDbReadTask newDbReadTaskInstance() {
        return new MySQLReadTask();
    }

    public class MySQLReadTask extends AbstractDbReadTask {
    	// 使用默认实现
    }
}
