package www.larkmidtable.com.model;

import lombok.Getter;
import lombok.Setter;
import www.larkmidtable.com.bean.ConfigBean;

/**
 * 参数对象父类
 * @Description 每种作业类型都有对应的参数。
 * @Author daizhong.liu
 * @Date 2022-11-24 14:30:16
 **/
@Getter
@Setter
public abstract class TaskParams {
    protected ConfigBean configBean;
}
