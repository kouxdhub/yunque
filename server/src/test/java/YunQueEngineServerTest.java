import com.larkmidtable.yunque.YunQueEngineServer;
import com.larkmidtable.yunque.rpc.ServerServiceImpl;
import org.junit.Test;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author Qing
 * @create 2023-10-15 16:03
 */
public class YunQueEngineServerTest {


   @Test
   public void test() throws UnknownHostException {
      ServerServiceImpl serverService = new ServerServiceImpl();
      String ip = "127.0.0.1" ;
      InetAddress ipaddress= InetAddress.getByName(ip);
      YunQueEngineServer.runRPCServer(serverService,12345 , ipaddress);

   }



}
