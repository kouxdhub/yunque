package www.larkmidtable.com;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import www.larkmidtable.com.channel.Channel;
import www.larkmidtable.com.log.LogRecord;
import www.larkmidtable.com.reader.AbstractDBReader;
import www.larkmidtable.com.util.DBType;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ForkJoinPool;

/**
 * @Date 2022/11/14 11:01
 **/
public class DB2Reader extends AbstractDBReader {
	private static final Logger logger = LoggerFactory.getLogger(DB2Reader.class);

    ForkJoinPool forkJoinPool;
    private Connection connection;



    @Override
    public void open() {
        forkJoinPool = new ForkJoinPool(Math.max(32, Runtime.getRuntime().availableProcessors()));
        LogRecord logRecord = LogRecord.newInstance();
        logRecord.start("DB2的Reader建立连接");
        try {
            Class.forName(DBType.DB2.getDriverClass());
            connection = DriverManager
                    .getConnection(configBean.getUrl(), configBean.getUsername(), configBean.getPassword());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        	logRecord.end();
        }
    }


    @Override
    public Queue<List<String>> startRead(String inputSplit) {
    	LogRecord logRecord = LogRecord.newInstance();
        logRecord.start("DB2读取数据操作");
        try {
            logger.info("执行的SQL:{}", inputSplit);
            defaultSingleStartRead(connection, inputSplit);
        } catch (Exception e) {
            e.printStackTrace();
        }
        logRecord.end();
        return Channel.getQueue();
    }

    @Override
    public Queue<List<String>> startRead(String[] inputSplits) {
        return null;
    }

    @Override
    public String[] createInputSplits(int count,int bcount) {
        logger.info("DB2的Reader开始进行分片开始....");
        String inputSql = String.format("select %s from %s", configBean.getColumn(), configBean.getTable());
        List<String> results = defaultInputSplits(configBean.getColumn(), inputSql);
        logger.info("DB2的Reader开始进行分片结束....");
        String[] array = new String[results.size()];
        return results.toArray(array);
    }

    @Override
    public List<String> defaultInputSplits(String column, String originInput) {
        List<String> splits = new ArrayList<>();
        int count = count();
        if (count > 0) {
            int threadSize = this.getConfigBean().getThread();
            int limitSize = count / configBean.getThread();
            int lastLimit = limitSize + count % configBean.getThread();
            for (int i = 0; i < threadSize; i++) {
                int limitStart = i * limitSize;
                int limitEnd = i == threadSize - 1 ? limitStart + limitSize : limitStart + lastLimit;
                String builder = "SELECT " + column + " FROM ( " + " " + originInput + " ) t" + " " + "LIMIT" +
                        " " + limitStart + "," + limitEnd;
                splits.add(builder);
            }
        } else {
            splits.add(originInput);
        }
        return splits;
    }

    @Override
    public void close() {
        try {
            logger.info("DB2的Reader开始进行关闭连接开始....");
            connection.close();
            logger.info("DB2的Reader开始进行关闭连接结束....");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected AbstractDbReadTask newDbReadTaskInstance() {
        return null; // TODO
    }

    @Override
    public int count() {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement =
                    connection.prepareStatement("SELECT count(*) FROM " + configBean.getTable());
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getInt(1);
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return 0;
    }
}
